<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";
require "../curl.php";



if (isset($_POST['submit'])) {

  try  {

    $connection = new PDO($dsn, $username, $password, $options);

    $user = new stdClass();
    $user->username = $_POST["correo"];
    $user->password = $_POST["pass"];
    $user->firstname = $_POST["nombre"];
    $user->lastname = $_POST["apellido"];
    $user->email = $_POST["correo"];

    $users = array($user);
    $params = array("users" => $users);

    $curl = new curl();

    $serverurl = "$webservice$service?wstoken=$token&moodlewsrestformat=$format&wsfunction=core_user_create_users";

    $usuario = json_decode($curl->post($serverurl,$params), true);

    if(array_key_exists($usuario,"exception") )
    {
        echo "error al procesar la información ".$usuario["message"];
        return;
    }

    $new_diplomado = array(
      "nombre" => $_POST['nombre'],
      "apellido" => $_POST["apellido"],
      "correo" => $_POST["correo"],
      "password" => $_POST["pass"],
      "moodle_id" => $usuario[0]["id"]
    );

    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "usuario",
      implode(", ", array_keys($new_diplomado)),
      ":" . implode(", :", array_keys($new_diplomado))
    );

    $statement = $connection->prepare($sql);
    $statement->execute($new_diplomado);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>

  <?php if (isset($_POST['submit']) && $statement) : ?>
    <blockquote><?php echo escape($_POST['nombre']); ?> successfully added.</blockquote>
  <?php endif; ?>

  <h2>Agregar usuario</h2>

  <form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <label for="firstname">Nombre</label>
    <input type="text" name="nombre" id="nombre"><br/>
    <label for="firstname">Apellido</label>
    <input type="text" name="apellido" id="apellido"><br/>
    <label for="firstname">Correo electronico</label>
    <input type="text" name="correo" id="correo"><br/>
    <label for="firstname">Contraseña</label>
    <input type="password" name="pass" id="pass"><br/>
    <label for="firstname">Repetir contraseña</label>
    <input type="password" name="passr" id="passr"><br/>
    <input type="submit" name="submit" value="Submit">
  </form>

  <a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
