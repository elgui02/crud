<?php

/**
 * Use an HTML form to edit an entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";
require "../curl.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  if( $_POST["password"] != $_POST["passr"])
  {
      //verificación
  }


  $passw = $_POST["password"];

  try {
    $connection = new PDO($dsn, $username, $password, $options);

    $userm = new stdClass();
    $userm->id = $_POST["moodle_id"];
    $userm->username = $_POST["correo"];
    $userm->firstname = $_POST["nombre"];
    $userm->lastname = $_POST["apellido"];
    $userm->email = $_POST["correo"];

    if( $passw )
    {
        $userm->password = $passw;
        $user =[
          "id"        => $_POST['id'],
          "nombre" => $_POST['nombre'],
          "apellido" => $_POST["apellido"],
          "correo" => $_POST["correo"],
          "moodle_id" => $_POST["moodle_id"],
          "password" => $passw
        ];
    }
    else {
        $user =[
          "id"        => $_POST['id'],
          "nombre" => $_POST['nombre'],
          "apellido" => $_POST["apellido"],
          "correo" => $_POST["correo"],
          "moodle_id" => $_POST["moodle_id"]
        ];
    }

    $pass = "";
    if( $passw )
    {
        $pass = ", password = :password ";
    }

    $sql = "UPDATE usuario
            SET nombre = :nombre,
            apellido = :apellido,
            correo = :correo,
            moodle_id = :moodle_id
            $pass
            WHERE id = :id";

  $statement = $connection->prepare($sql);
  $statement->execute($user);

  $users = array($userm);
  $params = array("users" => $users);
  $curl = new curl();
  $serverurl = "$webservice$service?wstoken=$token&moodlewsrestformat=$format&wsfunction=core_user_update_users";
  $usuario = json_decode($curl->post($serverurl,$params), true);

  var_dump($usuario);

  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
      var_dump($user);
  }
}

if (isset($_GET['id'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $id = $_GET['id'];

    $sql = "SELECT * FROM usuario WHERE id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $user = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "Something went wrong!";
    exit;
}
?>

<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
	<blockquote><?php echo escape($_POST['nombre']); ?> successfully updated.</blockquote>
<?php endif; ?>

<h2>Editar diplomado</h2>

<form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <?php foreach ($user as $key => $value) : ?>
      <?php if( $key != "usuario"): ?>
        <?php if( $key != "id" && $key != "moodle_id"): ?>
          <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
        <?php endif ?>
        <?php if( $key != "password"): ?>
	       <input type="<?php if( $key == "id" || $key == "moodle_id") echo "hidden"; else  echo "text";?>" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'id' ? 'readonly' : null); ?>>
            <br />
        <?php else: ?>
            <input type="password" name="password" id="password"><br/>
            <label for="firstname">Repetir contraseña</label>
            <input type="password" name="passr" id="passr"><br/>
            <br />
        <?php endif ?>
    <?php endif ?>
    <?php endforeach; ?>
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
