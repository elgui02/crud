<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";

if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    $connection = new PDO($dsn, $username, $password, $options);

    $new_diplomado = array(
      "nombre" => $_POST['nombre']
    );

    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "diplomado",
      implode(", ", array_keys($new_diplomado)),
      ":" . implode(", :", array_keys($new_diplomado))
    );

    var_dump($sql);
    die();

    $statement = $connection->prepare($sql);
    $statement->execute($new_diplomado);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>

  <?php if (isset($_POST['submit']) && $statement) : ?>
    <blockquote><?php echo escape($_POST['nombre']); ?> successfully added.</blockquote>
  <?php endif; ?>

  <h2>Agregar diplomado</h2>

  <form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <label for="firstname">Nombre</label>
    <input type="text" name="nombre" id="nombre">
    <input type="submit" name="submit" value="Submit">
  </form>

  <a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
