<?php

/**
 * Use an HTML form to edit an entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";
require "../curl.php";

if (isset($_GET['id'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $id = $_GET['id'];

    $sql = "SELECT * FROM diplomado WHERE id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $user = $statement->fetch(PDO::FETCH_ASSOC);

    // obtener listado de cursos por diplomado
    $sql = "SELECT * FROM curso_diplomado WHERE diplomado_id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();

    $cursos = $statement->fetchAll();

  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "Something went wrong!";
    exit;
}
?>

<?php require "templates/header.php"; ?>

<h2>Ver diplomado</h2>

<div class="col-lg-6">
    <table class="table">
        <?php foreach ($user as $key => $value) : ?>
    	    <tr>
                <th><?php echo $key; ?></th>
                <td><?php echo escape($value); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
<div class="col-lg-6">
    <h3>Listado cursos de diplomado</h3>
    <table class="table">
        <thead>
            <tr>
                <th>Curso</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $curl = new curl();
                $serverurl = "$webservice$service?wstoken=$token&moodlewsrestformat=$format&wsfunction=core_course_get_courses";
                $cursom = json_decode($curl->post($serverurl), true);
                $courses = array();
            ?>

            <?php foreach( $cursos as $curso ): ?>
                <tr>
                    <td>
                        <?php
                            foreach($cursom as $cm)
                            {
                                if( $cm["id"] == $curso["curso_id"] )
                                {
                                    $courses[]["courseid"] = $curso["curso_id"];
                                    echo $cm["fullname"];
                                }
                            }
                        ?>
                    </td>
                    <td></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<?php
$params = array("courses" => $courses);
$serverurl = "$webservice$service?wstoken=$token&moodlewsrestformat=$format&wsfunction=block_micoope_diplomado_get_courses";
$usuarios = json_decode(json_decode($curl->post($serverurl,$params), true), true);
?>
<table class="table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Estado</th>
            <th>Curso</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($usuarios as $usuario): ?>
            <tr>
                <td><?php echo $usuario["nombre"]; ?></td>
                <td><?php echo $usuario["email"]; ?></td>
                <td><?php echo $usuario["estado"]; ?></td>
                <td><?php echo $usuario["curso"]; ?></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Agregar curso
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="new_curso.php" method="POST">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar curso a diplomado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <input type="hidden" name="diplomado" id="diplomado" value=<?php echo $_GET['id'] ?> >
            <table>
                <tr>
                    <th>Curso</th>
                    <td>
                        <select name="curso" id="curso">
                        <?php
                            /// REST CALL

                            foreach($cursom as $cm)
                            {
                                if( $cm["categoryid"] > 0)
                                {
                                    $id = $cm["id"];
                                    $nombre = $cm["fullname"];
                                    echo "<option value='$id'>$nombre</option>";
                                }
                            }
                        ?>
                        </select>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Agregar</button>
          </div>
        </div>
    </form>
  </div>
</div>

<a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
