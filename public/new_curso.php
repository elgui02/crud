<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";
require "../curl.php";

$curso = $_POST["curso"];

try  {
$connection = new PDO($dsn, $username, $password, $options);

$new_diplomado = array(
  "diplomado_id" => $_POST['diplomado'],
  'curso_id' => $_POST["curso"]
);

$sql = sprintf(
  "INSERT INTO %s (%s) values (%s)",
  "curso_diplomado",
  implode(", ", array_keys($new_diplomado)),
  ":" . implode(", :", array_keys($new_diplomado))
);

$statement = $connection->prepare($sql);
$statement->execute($new_diplomado);
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}

if ( $statement)
    echo " successfully added.";
?>
