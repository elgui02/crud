<?php
require "../config.php";
require "../common.php";

try {
  $connection = new PDO($dsn, $username, $password, $options);

  $sql = "SELECT * FROM diplomado";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>
<?php require "templates/header.php"; ?>

<h2>Lista de diplomados</h2>

<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
        <tr>
            <td><?php echo escape($row["id"]); ?></td>
            <td><?php echo escape($row["nombre"]); ?></td>
            <td>
				<ul>
					<li><a href="update-single.php?id=<?php echo escape($row["id"]); ?>">Editar</a></li>
					<li><a href="show.php?id=<?php echo escape($row["id"]); ?>">Ver</a></li>
				</ul>
			</td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<ul>
	<li><a href="create.php"><strong>Nuevo</strong></a> - agregar diplomado</li>
	<!-- <li><a href="read.php"><strong>Read</strong></a> - find a user</li>
	<li><a href="update.php"><strong>Update</strong></a> - edit a user</li>
	<li><a href="delete.php"><strong>Delete</strong></a> - delete a user</li> -->
</ul>

<?php include "templates/footer.php"; ?>
