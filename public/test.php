<?php

/**
 * Use an HTML form to edit an entry in the
 * users table.
 *
 */

require "../config.php";
require "../common.php";
require "../curl.php";

$functionname = 'core_user_update_users';
// REST RETURNED VALUES FORMAT
$restformat = 'json'; //Also possible in Moodle 2.2 and later: 'json'
                     //Setting it to 'json' will fail all calls on earlier Moodle version
//////// moodle_user_create_users ////////
/// PARAMETERS - NEED TO BE CHANGED IF YOU CALL A DIFFERENT FUNCTION
$user1 = new stdClass();
$user1->id = 3;
$user1->username = 'testusername1';
$user1->password = 'testpassword2L!';
$user1->firstname = 'usuario';
$user1->lastname = 'prueba';
$user1->email = 'testemail1@moodle.com';
$users = array($user1);
$params = array('users' => $users);
/// REST CALL
header('Content-Type: text/plain');
$serverurl = "$webservice$service?wstoken=$token&wsfunction=$functionname";
var_dump($serverurl);
var_dump($params);
$curl = new curl;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
$resp = $curl->post($serverurl . $restformat, $params);

var_dump($resp);
