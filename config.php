<?php

/**
 * Configuration for database connection
 *
 */

$host       = "localhost";
$username   = "diplomado";
$password   = "diplomado";
$dbname     = "diplomado";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
$webservice = "http://www.corp.sourcetip.com/moodle/";
$service ="webservice/rest/server.php";
$token = "70fda5354ba14b08da167b8eed565dcf";
$format = "json";
